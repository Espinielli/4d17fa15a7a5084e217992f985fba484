This example belongs to [the presentation](rveciana.github.com/geoexamples/geoinquiets/d3js/index_en.html) made to the [OSGeo](http://www.osgeo.org/) local group in Barcelona [Geoinquiets](http://www.geoinquiets.cat/)
The example is an adaptation from [Mike Bostock](http://bost.ocks.org/mike/)'s [Symbol Map example](http://bl.ocks.org/mbostock/4342045)

Forked from <a href='http://bl.ocks.org/rveciana/'>rveciana</a>'s block: <a href='http://bl.ocks.org/rveciana/5181105'>D3 tutorial V: Adding tooltips</a> in order to retrieve relevant gist files via `rawgit`.